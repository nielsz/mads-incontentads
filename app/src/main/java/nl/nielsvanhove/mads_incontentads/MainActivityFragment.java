package nl.nielsvanhove.mads_incontentads;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.MadsAdView.MadsAdView;
import com.commonsware.cwac.merge.MergeAdapter;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    MergeAdapter adapter;
    private ListView mListView;


    public MainActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View inflate = inflater.inflate(R.layout.fragment_main, container, false);

        mListView = (ListView)inflate.findViewById(R.id.list);
        return inflate;
    }


    @Override public void onResume() {
        super.onResume();
        System.out.println("MainActivityFragment.onResume");
        displayPage();
    }

    public void displayPage() {
        adapter = new MergeAdapter();

        for(int i = 1;i< 100; i++) {
            TextView textView = new TextView(getActivity());
            AbsListView.LayoutParams wrapParams = new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT, 170);
            textView.setLayoutParams(wrapParams);
            textView.setText("number " + i);
            if(i % 2 == 0) {
                textView.setBackgroundColor(Color.GRAY);
            }

            if(i % 5 ==0) {
                adapter.addView(getMads());
            }




            adapter.addView(textView);
        }


        mListView.setAdapter(adapter);
    }

    private View getMads() {

        LinearLayout mIncontentWrapper = new LinearLayout(getActivity());
        mIncontentWrapper.setGravity(Gravity.CLIP_HORIZONTAL);
        mIncontentWrapper.setBackgroundColor(Color.GREEN);
        AbsListView.LayoutParams wrapParams = new AbsListView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mIncontentWrapper.setLayoutParams(wrapParams);


        String tvgids_homepage = "3436965151";
        String mads_demo = "7345907052";
        String tvgids_movies_incontent = "6451311030";

        MadsAdView adView = new MadsAdView(getActivity(), tvgids_movies_incontent);
        adView.setMadsAdType(MadsAdView.TYPE_AD_VIEW_INLINE);
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        adView.setLayoutParams(lp);
        adView.update();

        mIncontentWrapper.removeAllViews();
        mIncontentWrapper.addView(adView);

        return mIncontentWrapper;

    }


}
